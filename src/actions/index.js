export function selectPatient(patient) {
  // this is an ActionCreator, so it must return an action object with a
  // type property.
  return {
    type: 'PATIENT_SELECTED',
    payload: patient
  }
}

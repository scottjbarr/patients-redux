import React, { Component } from 'react'

import PatientList from '../containers/patient_list'
import PatientDetail from '../containers/patient_detail'

export default class App extends Component {
  render() {
    return (
      <div>
        <PatientList />
        <PatientDetail />
      </div>
    )
  }
}

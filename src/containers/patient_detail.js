import React, { Component } from 'react'
import { connect } from 'react-redux'

class PatientDetail extends Component {
  render() {
    if (!this.props.patient) {
      return <div>Select a Patient.</div>
    }
    return (
      <div className="col-sm-8">
        <h3>Patient</h3>
        <div>Name: {this.props.patient.name}</div>
        <div>DOB: {this.props.patient.dob.toLocaleDateString('en-au')}</div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    patient: state.activePatient
  }
}

export default connect(mapStateToProps)(PatientDetail)

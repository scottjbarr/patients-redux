import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { selectPatient } from '../actions/index'

class PatientList extends Component {
  renderList() {
    return this.props.patients.map((patient) => {
      return (
        <li
            key={patient.id}
            onClick={() => this.props.selectPatient(patient)}
            className="list-group-item">
          {patient.name}
        </li>
      )
    })
  }

  render() {
    return (
      <ul className="list-group col-sm-4">
        {this.renderList()}
      </ul>
    )
  }
}

function mapStateToProps(state) {
  // whatever is returned will show up as props inside PatientList
  return {
    patients: state.patients
  }
}

// anything returned from this function will end up as props on the PatientList
// container
function mapDispatchToProps(dispatch) {
  // whenever selectPatient is called the result should be passed to all
  // reducers
  return bindActionCreators({ selectPatient: selectPatient }, dispatch)
}

// promote PatientList from a component to a container as it needs to know about
// the dispatch method, selectPatient. Make it available as a prop.
export default connect(mapStateToProps, mapDispatchToProps)(PatientList)

import { combineReducers } from 'redux'

import PatientsReducer from './reducer_patients'
import ActivePatient from './reducer_active_patient'

const rootReducer = combineReducers({
  patients: PatientsReducer,
  activePatient: ActivePatient
})

export default rootReducer

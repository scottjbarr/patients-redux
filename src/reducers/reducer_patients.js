export default function() {
  return [
    {
      id: '570d790e-f757-11e6-9565-5fbbeffa0fd6',
      first_name: 'Scott',
      last_name: 'Barr',
      name: 'Scott Barr',
      dob: new Date(1922, 2, 22)
    },
    {
      id: '65901e8c-f757-11e6-adfb-67b16f14217e',
      first_name: 'Nigel',
      last_name: 'McBryde',
      name: 'Nigel McBryde',
      dob: new Date(1988, 8, 18)
    }
  ]
}
